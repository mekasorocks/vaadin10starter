# vaadin10starter

First steps with Vaadin 10

+ (responsive) layout with FlexLayout and CSS
+ MVP architecture
+ localization with I18NProvider and ResourceBundles
+ Templating with RouterLayout
