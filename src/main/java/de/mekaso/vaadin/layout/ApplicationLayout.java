package de.mekaso.vaadin.layout;

import java.util.Locale;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcons;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.page.BodySize;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

import de.mekaso.vaadin.view.AddressView;
import de.mekaso.vaadin.view.PaymentView;
import de.mekaso.vaadin.view.PersonView;
import de.mekaso.vaadin.view.SummaryView;

@Theme(Lumo.class)
@PageTitle("Vaadin10 Starter Project")
@BodySize(height = "100vh", width = "100vw")
@StyleSheet("applicationlayout.css")
public class ApplicationLayout extends FlexLayout implements RouterLayout {
	private static final long serialVersionUID = 1L;

	private Div header;
	private Div contentContainer;
	private Div footer;
	
	@Inject
	private I18NProvider i18nProvider;
	@Inject
	private Event<LocaleChangeEvent> localeChanger;
	
	@PostConstruct
	private void init() {
		super.setId("applicationlayout");
		// navigation
		Div navigation = createNavigation();
		navigation.addClassName("navigation");
		// locale switch
		Div localeButtons = new Div();
		localeButtons.addClassName("localeswitch");
		for (Locale locale : this.i18nProvider.getProvidedLocales()) {
			Button localeButton = new Button(locale.getLanguage());
			localeButton.addClickListener(click -> {
				if (this.getUI().isPresent()) {
					this.getUI().get().setLocale(locale);
					this.localeChanger.fire(new LocaleChangeEvent(this.getUI().get(), locale));
				}
			});
			localeButtons.add(localeButton);
		}
		// header with navigation and locale switch
		this.header = new Div(navigation, localeButtons);
		this.header.addClassName("header");
		// the content
		this.contentContainer = new Div();
		this.contentContainer.addClassName("content");
		// sticky footer
		this.footer = new Div();
		this.footer.addClassName("footer");
		this.footer.add(new Text("Vaadin 10 Starterproject 2018"));
		add(this.header);
		add(this.contentContainer);
		add(this.footer);
	}

	private Div createNavigation() {
		RouterLink personLink = new RouterLink(null, PersonView.class);
		personLink.add(new Button("Person", new Icon(VaadinIcons.ANCHOR)));
		RouterLink addressLink = new RouterLink(null, AddressView.class);
		addressLink.add(new Button("Address", new Icon(VaadinIcons.ADD_DOCK)));
		RouterLink paymentLink = new RouterLink(null, PaymentView.class);
		paymentLink.add(new Button("Payment", new Icon(VaadinIcons.BOOKMARK)));
		RouterLink summaryLink = new RouterLink(null, SummaryView.class);
		summaryLink.add(new Button("Summary", new Icon(VaadinIcons.CLOSE_BIG)));
		return new Div(personLink, addressLink, paymentLink, summaryLink);
	}
	
	@Override
	public void showRouterLayoutContent(HasElement content) {
		if (content != null) {
			this.contentContainer.removeAll();
            this.contentContainer.add(Objects.requireNonNull((Component)content));
        }
	}
}
